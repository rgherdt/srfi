# Collection of SRFI implementations

*This is not an official Scheme SRFI repository. For reference implementations,
please look at the corresponding references at https://srfi.schemers.org/*

Scheme SRFI implementations in portable R7RS scheme. The focus for now is to
get a collection of SRFI's working for Gambit, specially those needed by my
https://codeberg.org/rgherdt/scheme-lsp-server work.

