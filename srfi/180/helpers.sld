(define-library (srfi 180 helpers)

  (export %read-error? valid-number?)

  (import (scheme base)
          ;;(chibi ast)
          )
   (cond-expand
    (guile (import (rx irregex)))
    (chicken (import (chicken irregex)))
    (else (import (chibi irregex))))
  (begin

    (define (%read-error? x)
      (and (error-object? x) (memq (exception-kind x) '(user read read-incomplete)) #t))

    (define (valid-number? string)
      ;; based on https://stackoverflow.com/a/13340826/140837
      (irregex-search '(seq
                        (? #\-)
                        (or #\0 (seq (- numeric #\0)
                                     (* numeric)))
                        (? (seq #\. (+ numeric)))
                        (? (seq (or #\e #\E)
                                (? (or #\- #\+))
                                (+ numeric))))
                      string))))
