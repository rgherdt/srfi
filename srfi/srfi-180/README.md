# SRFI 180

Implementation adapted from the reference one:

https://github.com/scheme-requests-for-implementation/srfi-180

Changes to original implementation:

- Removed (unused?) dependencies:
  - removed dependency on (scheme text)
  - removed dependency on (chibi ast)
  - removed dependency on (check)
- replace dependency on (chibi regexp) by the portable (chibi irregex)
