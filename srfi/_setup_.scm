(define-module-alias (chibi irregex)
  (github.com/ashinn/irregex))

(define-module-alias (srfi 145)
  (codeberg.org/rgherdt/srfi srfi 145))

(define-module-alias (srfi 180)
  (codeberg.org/rgherdt/srfi srfi 180))

